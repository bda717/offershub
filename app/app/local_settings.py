# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
DOCKER = True
HOST = 'postgres' if DOCKER else '127.0.0.1'
ALLOWED_HOSTS = ['*']
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'offershub',
        'USER': 'root',
        'PASSWORD': 'root',
        'HOST': 'postgres',
        'PORT': '5432',
    }
}
ASANA_TOKEN = ''
