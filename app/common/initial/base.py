from abc import ABCMeta, abstractproperty

from django.core.management.base import CommandError


def update_or_create(model, defaults=None, **kwargs):
    try:
        obj, created = model.objects.update_or_create(defaults=defaults, **kwargs)
    except Exception as e:
        raise CommandError('%r does not updated or created.\n'
                           'Error: %s' % (model.__name__, e))
    else:
        act = 'created' if created else 'updated'
        print('Successfully %s %r' % (act, obj))
        return obj


class InitialModel(metaclass=ABCMeta):
    help = 'Initial data for model'

    @abstractproperty
    def model(self):
        pass

    @abstractproperty
    def initial(self):
        """
        Initial is an iterative object of a dictionary (field, value) used to update or create the model.
        """
        pass

    def __init__(self, *args, **kwargs):
        self.handle()

    def update_or_create(self, initial):
        for defaults in initial:
            gid = defaults.pop('gid')
            update_or_create(self.model, defaults, gid=gid)

    def handle(self, *args, **options):
        try:
            self.model._meta.model_name
        except AttributeError as e:
            raise NameError('Model is not defined')
        else:
            self.update_or_create(self.initial)
