from common.initial.register import register, InitialRegister
from common.initial.base import InitialModel

__all__ = ['register', 'InitialRegister', 'InitialModel']
