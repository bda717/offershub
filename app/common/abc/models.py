from django.db import models
from django.utils.translation import ugettext_lazy as _


class AbcGid(models.Model):
    gid = models.BigIntegerField(_('Идентификатор'), unique=True, db_index=True, editable=False)

    class Meta:
        abstract = True


class AbcName(models.Model):
    name = models.CharField(_('Название'), max_length=255)

    class Meta:
        abstract = True

    def __str__(self):
        return self.name
