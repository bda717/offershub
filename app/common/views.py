from django.http import HttpResponseRedirect
from django.utils.translation import LANGUAGE_SESSION_KEY, activate, get_language_from_request
from django.views.generic import TemplateView
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
# from django.views.generic.base import View
# from django.db.models import Q
# from config.models import Config, ConfigFile
# # from feedback.views import order_modal_back
# from feedback.forms import *
# from catalog.forms import SearchForm
# from pages.models import Article, Section
# from tags.models import Tag
# from django.conf import settings
# from .functions import *
# from django.shortcuts import render
from django.db.models import Count, Q

# from common.menus.models import *
# from catalog.models import Category


def form_context(request):
    context = {
        # 'form_callback_modal': callback_modal_back(request),  # форма "Обратный звонок" (модальное) CallBackForm()
        # 'form_participant_modal': participant_modal_back(request),  # форма "Стать участником" (модальное)
        # 'form_tours_modal': tours_modal_back(request),
        # 'form_participant': participant_back(request),  # форма "Стать участником"
        # 'form_contact_us': contact_us_back(request),  # форма "Свяжитесь с нами" ContactUsForm()
        # 'form_order_modal': order_modal_back(request),
        # 'form_your_questions': your_questions_back(request),  # форма "Ваши вопросы"
        # 'form_reviews_modal': reviews_modal_back(request),  # форма "Оставить  отзыв" (модальное)
        # 'msg_form': msg_form_back(request),  # сообщение отправки
    }
    return context


# def data_context(request):
#     section_news = get_object_or_none(Section, slug='news')
#
#     tag_ids = Tag.get_tag_ids(['news_main'])
#     article_news_main = Article.objects.filter(attached_tags__tag_id__in=tag_ids).order_by('-date') if tag_ids else None
#
#     context = {'article_news_main': article_news_main, 'section_news': section_news}
#     return context


# def base_context(request):
#     context = {
#         'MEDIA_URL': settings.MEDIA_URL,
#         'DEBUG': settings.DEBUG,
#         'user': request.user,
#         'DOMAIN': Config.get_domain(),
#     }
#     context.update(get_common_context(request))
#     context.update(data_context(request))
#     context.update(form_context(request))
#     return context

# from django.core.urlresolvers import resolve

class BaseTemplateView(TemplateView):
    base_template_name = 'base.html'
    # decorators = []

    # def __init__(self, *args, **kwargs):
    # #     super(BaseView, self).__init__(**kwargs)
    #     self.breadcrumbs = [{'title': _('Главная'), 'url': reverse('home')}]

    def get_context_data(self, **kwargs):
        context = super(BaseTemplateView, self).get_context_data(**kwargs)
        # categories_sidebar = Category.get_active().annotate(products_count=Count('product', filter=Q(product__is_active=True)))
        # Settings context data for base template
        # context['default_language'] = self.request.session.get('default_language', settings.LANGUAGE_CODE)

        # context['call_form'] = CallForm()
        # context['question_form'] = QuestionForm()
        # context['question_modal_form'] = QuestionModalForm()
        # if self.request.GET.get('q'):
        #     context['search_form'] = SearchForm({'q': self.request.GET.get('q')})
        # else:
        #     context['search_form'] = SearchForm()

        # context.update(base_context(self.request))
        context.update(
            base_template_name=self.base_template_name,
            # categories_sidebar=categories_sidebar,
        )
        return context

    # def search_query(self, products, search_key):
    #     if search_key:
    #         return products.filter(Q(title__icontains=search_key) |
    #                                Q(color__icontains=search_key) |
    #                                Q(top__icontains=search_key) |
    #                                Q(foot__icontains=search_key) |
    #                                Q(lining__icontains=search_key))
    #     return products


    #
    # @classmethod
    # def get_decorators(cls):
    #     """
    #     Returns list of decorators defined as attribute of class
    #     Generic base views should override get_decorators method instead of defining decorators attribute
    #     """
    #     return cls.decorators
    #
    # @classmethod
    # def as_view(cls, **initkwargs):
    #     """
    #     Returns view function
    #     Decorators will be applied defined at class level
    #     """
    #     view = super(BaseView, cls).as_view(**initkwargs)
    #     # Applying decorators to generated view
    #     for decorator in cls.get_decorators():
    #         view = decorator(view)
    #     return view


# def get_common_context(request):
#     context = dict()
#
#     context['settings'] = settings
#     context['config'] = Config.get_config_list()
#     context['configfile'] = ConfigFile.get_config_list()
#
#     if not request.is_ajax():
#         pass
#
#     return context
#
#
# def page_not_found(request):
#     return render(request, '404.html', get_common_context(request), status=404)
#
#
# # class ChangeLanguageView(View):
# #     """
# #     Change user's default language and redirect to current page
# #     """
# #
# #     def post(self, request):
# #         next_url = request.POST['next'] or reverse('main_index')
# #         request.session['default_language'] = request.POST['default_language']
# #         return HttpResponseRedirect(next_url)
#
#
# def set_language_from_url(request, user_language, slug):
#     lang = get_language_from_request(request)
#     if lang != 'ru':
#         pure_slug = '/' + slug.replace('/' + lang + '/', user_language + '/')
#     else:
#         pure_slug = '/' + user_language + slug
#     request.session[LANGUAGE_SESSION_KEY] = user_language
#     return HttpResponseRedirect(pure_slug)