import os

# from django.core.management import call_command
from django.core.management.base import BaseCommand, CommandError

# from app.settings import BASE_DIR


class Command(BaseCommand):
    help = 'Remove migrations'

    def add_arguments(self, parser):
        parser.add_argument(
            'app_names',
            nargs='*',
            help='App names'
        )
        parser.add_argument(
            '-y',
            '--yes',
            action='store_true',
            help='Automatic yes to prompts'
        )

    def del_migrations(self, app_names, all=None):
        """
        Remove apps migrations
        :type args: App names
        :type all: All app names
        """
        exclude_dirs = ('env', '.idea', '.git', '__pycache__')
        exclude_files = ('env', '.idea', '.git', '__pycache__')

        if app_names:
            print('App names or argument --all are not defined')
        else:
            try:
                # Remove all migrations, exclude 'env' dir for Linux
                os.system('find . -path "*/migrations/*.py" -not -path "./env/*" -not -name "__init__.py" -delete')
            except Exception as e:
                print(e)
            # _, dirs, _ = next(os.walk(BASE_DIR))
            # for app in dirs:
            #     if app not in exclude_dirs:
            #         print(os.walk(app))

    def handle(self, *args, **options):
        if options['yes']:
            self.del_migrations(options['app_names'])
        else:
            while True:
                query = input('Do you want to remove all migrations of %s? [Y/n]' % '')
                if query == '' or not query.lower() in ['yes', 'y', 'no', 'n']:
                    print('Please answer with yes or no!')
                else:
                    if query.lower()[0] == 'y':
                        self.del_migrations(options['app_names'])
                    break
