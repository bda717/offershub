import asana
from django.conf import settings


class Client:
    auth_token = settings.ASANA_TOKEN
    client = asana.Client.access_token(auth_token)

    @property
    def user(self):
        return self.client.users.me()

    def get_workspaces(self):
        return self.user.get('workspaces')

    def get_projects(self):
        for workspace in self.get_workspaces():
            for projects in self.client.projects.find_all({'workspace': workspace['gid']}):
                projects['workspace_gid'] = workspace.get('gid')
                yield projects

    def update_project(self, gid, **kwargs):
        self.client.projects.update(gid, kwargs)

    def create_project(self, workspace_gid, **kwargs):
        return self.client.projects.create_in_workspace(workspace_gid, kwargs)

    def get_tasks(self):
        for project in self.get_projects():
            for task in self.client.tasks.find_by_project(project['gid'], opt_fields=['gid', 'assignee', 'name', 'projects', 'workspace']):
                yield task

    def update_task(self, gid, name, user_gid):
        self.client.tasks.update(gid, {'name': name, 'assignee': user_gid})

    def create_task(self, name, user_gid, projects_gid):
        return self.client.tasks.create({'name': name, 'assignee': user_gid, 'projects': projects_gid})

    def get_users(self):
        for workspace in self.get_workspaces():
            yield from self.client.users.find_all({'workspace': workspace['gid']})

    def __repr__(self):
        return '<%s: %s, %s>' % (self.__class__.__name__, self.user.get('gid'), self.user.get('name'))

    def __str__(self):
        return self.__repr__()
