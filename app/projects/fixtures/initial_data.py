from common.initial import InitialModel, register
from projects.models import Project, Task, User, Workspace
from common.asana.client import Client

client = Client()


@register
class InitialModelWorkspace(InitialModel):
    model = Workspace

    @property
    def initial(self):
        for workspace in client.get_workspaces():
            yield {'gid': workspace['gid'], 'name': workspace['name']}


@register
class InitialModelProject(InitialModel):
    model = Project

    @property
    def initial(self):
        for project in client.get_projects():
            workspace = Workspace.objects.get(gid=project['workspace_gid'])
            yield {'gid': project['gid'], 'name': project['name'], 'workspace': workspace}


@register
class InitialModelUser(InitialModel):
    model = User

    @property
    def initial(self):
        for user in client.get_users():
            yield {'gid': user['gid'], 'name': user['name']}


@register
class InitialModelTask(InitialModel):
    model = Task

    @property
    def initial(self):
        for task in client.get_tasks():
            project = Project.objects.get(gid=task['projects'][0]['gid']) if task['projects'] else None
            user = User.objects.get(gid=task['assignee']['gid']) if task['assignee'] else None
            yield {'gid': task['gid'], 'name': task['name'], 'project': project, 'user': user}
