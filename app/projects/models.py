from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import ugettext_lazy as _

from common.abc import AbcGid, AbcName
from common.asana import Client


class Workspace(AbcGid, AbcName):
    class Meta:
        verbose_name = _('Рабочая область')
        verbose_name_plural = _('Рабочие области')


class Project(AbcGid, AbcName):
    workspace = models.ForeignKey(Workspace, verbose_name=_('Рабочая область'), null=True, on_delete=models.SET_NULL)

    class Meta:
        verbose_name = _('Проект')
        verbose_name_plural = _('Проекты')

    def clean(self):
        try:
            client = Client()
            if self.id:
                client.update_project(self.gid, name=self.name)
            elif self.workspace and self.name:
                project = client.create_project(str(self.workspace.gid), name=self.name)
                self.gid = project['gid']
        except Exception as e:
            raise ValidationError(e)


class User(AbcGid, AbcName):
    name = models.CharField(_('Имя'), max_length=255)

    class Meta:
        verbose_name = _('Исполнитель')
        verbose_name_plural = _('Исполнители')


class Task(AbcGid, AbcName):
    project = models.ForeignKey(Project, verbose_name=_('Проект'), null=True, on_delete=models.SET_NULL)
    user = models.ForeignKey(User, verbose_name=_('Исполнитель'), null=True, blank=True, on_delete=models.SET_NULL)
    name = models.CharField(_('Текст задачи'), max_length=255)

    class Meta:
        verbose_name = _('Задача')
        verbose_name_plural = _('Задачи')

    def clean(self):
        try:
            client = Client()
            if self.id:
                user_gid = str(self.user.gid) if self.user else None
                client.update_task(self.gid, self.name, user_gid)
            else:
                user_gid = str(self.user.gid) if self.user else None
                projects_gid = str(self.project.gid) if self.project else None
                task = client.create_task(self.name, user_gid, projects_gid)
                self.gid = task['gid']
        except Exception as e:
            raise ValidationError(e)
