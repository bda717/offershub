from django.contrib import admin

from projects.models import Project, User, Task


@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = ('gid', 'name', 'workspace')
    fields = ('gid', 'workspace', 'name')
    readonly_fields = ('gid',)
    list_filter = ('workspace',)


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ('gid', 'name')
    readonly_fields = ('gid', 'name')

    # def has_add_permission(self, request, obj=None):
    #     return False


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = ('gid', 'name', 'project', 'user')
    fields = ('gid', 'project', 'user', 'name')
    readonly_fields = ['gid']
    list_filter = ('project', 'user')

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return self.readonly_fields + ['project']
        return self.readonly_fields
