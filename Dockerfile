FROM python:3.7
ENV PYTHONUNBUFFERED 1
RUN mkdir -p /src/app
COPY ./app /src/app
COPY wait-for-it.sh /src
RUN chmod +x /src/wait-for-it.sh
WORKDIR /src/app
RUN pip install -r requirements.txt
